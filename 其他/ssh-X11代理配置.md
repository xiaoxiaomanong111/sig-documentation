# ssh-X11代理配置

## 背景
SSH的X11 Forwarding功能主要是可以提供一个解决方案，从而可以在本机客户端上运行服务器端的一些gui程序，而非cli程序，从而更高效的完成相应代码的开发。

一个比较简单的例子是可以在远端服务器配置一个xclock，并且在本地运行该程序。

## 配置
### 远程开发机
在远程服务器上启用X11 Forwarding。在OpenSSH的配置文件中`/etc/ssh/sshd_config`，打开如下两项：
```sh
AllowTcpForwarding yes
X11Forwarding yes
```

之后运行
```sh
systemctl restart ssh.service #Debian
systemctl restart sshd.service #Other Linux
```
重启sshd服务，即配置完成远端服务器

### 本地配置
在本地环境下，对OpenSSH-Client的配置文件`/etc/ssh/ssh_config`修改，打开如下三项：
```sh
ForwardAgent yes
ForwardX11 yes
ForwardX11Trusted yes
```

之后运行
```sh
systemctl restart ssh.service #Debian
systemctl restart sshd.service #Other Linux
```
重启sshd服务，即配置完成。

## 示例——xclock配置
因为我的远端服务器是fedora系统，所以运行以下命令安装xclock。
```sh
sudo dnf install -y xclock
```

之后运行`xclock`，可以看到clock的gui界面，说明配置成功。
![clock](../assets/ssh-X11代理配置/clock.png)

## Ref
本文撰写中参考以下网站: 
[https://www.jianshu.com/p/24663f3491fa](https://www.jianshu.com/p/24663f3491fa)