# openkylin基础 设置 查看默认应用
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

设置 - 应用 - 默认应用

![image](images/ok-defaultsf-1.png)

日常使用中，我们安装了新软件，但是双击文件运行时发现程序还是之前旧的，此时可以通过修改打开文件的默认应用来解决这一问题。

&emsp;


