# nm-connection-editor 打开网络连接配置工具
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;


``` bash
nm-connection-editor
```

![image](images/ok-bash-nm-connection-editor-1.png)

在这个界面可以对具体的网络链接进行配置

&emsp;

