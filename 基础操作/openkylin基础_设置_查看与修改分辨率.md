# openkylin基础 设置 查看与修改分辨率
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

左下角logo - 设置

![image](images/ok-screen-1.png)

设置 - 系统

![image](images/ok-screen-2.png)

显示器 - 分辨率

![image](images/ok-screen-3.png)

&emsp;


