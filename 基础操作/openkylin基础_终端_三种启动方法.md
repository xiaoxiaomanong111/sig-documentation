# openkylin基础 终端 三种启动方法
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

左下角logo - MATE终端

![image](images/ok-openterminal-1.png)

![image](images/ok-openterminal-2.png)

桌面右键 - 打开终端

![image](images/ok-openterminal-3.png)

![image](images/ok-openterminal-4.png)

快捷键 ctrl + alt + t

![image](images/ok-openterminal-5.png)

&emsp;


