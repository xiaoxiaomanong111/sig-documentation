# openkylin基础 设置 查看网络配置
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

设置 - 网络 - 有线网络

![image](images/ok-shownetwork-1.png)

查看正在使用的网络配置信息

![image](images/ok-shownetwork-2.png)

网络配置信息 ipv4

![image](images/ok-shownetwork-3.png)

网络配置信息 ipv6

![image](images/ok-shownetwork-4.png)

回到主界面 - 高级设置

![image](images/ok-shownetwork-5.png)

![image](images/ok-shownetwork-6.png)

查看常规 以太网 802.1X安全性等信息

![image](images/ok-shownetwork-7.png)

&emsp;


