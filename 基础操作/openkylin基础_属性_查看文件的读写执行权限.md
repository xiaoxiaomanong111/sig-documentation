# openkylin基础 属性 查看文件的读写执行权限
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;


右键文件 - 属性

![image](images/ok-showfilepri-1.png)

查看文件权限

![image](images/ok-showfilepri-2.png)

权限有 读、写、执行

分类有 用户、用户所在的组，其他

&emsp;

