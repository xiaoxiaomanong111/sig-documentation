# 查看系统信息

- 查看内核版本

```
uname -a
```

- 查看系统版本信息

```
cat /etc/os-release
lsb_release -a
```

- 查看CPU信息

```
lscpu
```

- 查看内存信息

```
free
```

- 查看磁盘信息

```
df -h
fdisk -l
```

- 查看系统资源实时信息

```
top
```

- 查看硬盘读写速度
```
iostat -t 1 3
# 1s一次，查看3次
```


> 版权声明：本文为rechie原创，依据 [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) 许可证进行授权，转载请附上出处链接及本声明。