# openkylin基础 设置 纯色背景
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

设置 - 个性化 - 背景

![image](images/ok-screen-color-1.png)

复古风，仿XP。
一些检测显示器是否漏光的软件也会使用纯色背景

&emsp;

