# openkylin基础——查看时区和时间同步服务器

&emsp;
当前操作系统版本：openKylin-0.9.5-x86_64

### 一、添加其他时区时间

设置 - 时间语言 - 时间和日期

![image](images/ok-timeconserver-1-new.png)

添加其他时区

![image](images/ok-timeconserver-2-new.png)

通过"添加"功能，可以查看其他时区的时间，但是在桌面却看不到，只能在时间日期窗口中进行查看。

### 二、查看系统时间

Linux的时间分为系统时间和硬件时间，系统时间指的是当前Linux内核中的时间，硬件时间指的是主板上BIOS重点时间，由主板电池供电来维持运行，有时系统时间与硬件时间可能是不同步的。

查看系统时间：

```
$ date
```

查看硬件时间：

```
$ sudo hwclock
```

### 三、开启或关闭网络时间同步

```
$ time datectl set_ntp true/false
```

在弹出的对话框输入密码即可

### 四、修改系统时间

1. 修改系统时间：

   ```
   $ sudo date -s 月/日/年 时:分:秒
   ```

2. 更新硬件时间（将系统时间写入硬件时间）：

   ```
   $ sudo hwclock -w
   ```

3. 或者直接以硬件时间更新系统时间

   ```
   $ sudo hwclock -s
   ```


&emsp;