# openkylin基础 归档管理器 压缩与解压缩
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;


![image](images/ok-zip-1.png)

归档管理器是一个压缩与解压缩的工具

![image](images/ok-zip-2.png)

单个压缩包进行解压缩

![image](images/ok-zip-3.png)

多个文件进行压缩

![image](images/ok-zip-4.png)

设置密码与拆分

![image](images/ok-zip-5.png)

&emsp;

