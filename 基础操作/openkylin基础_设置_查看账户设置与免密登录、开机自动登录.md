# openkylin基础 设置 查看账户设置与免密登录、开机自动登录
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

设置 - 账户 - 账户信息

![image](images/ok-user-1.png)

新建用户及相关字段要求

![image](images/ok-user-2.png)

修改密码

![image](images/ok-user-3.png)

查看用户组

![image](images/ok-user-4.png)

![image](images/ok-user-5.png)

&emsp;


