# openkylin基础 设置 全局搜索
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

通过开启索引提高全局搜索效率

![image](images/ok-search-v-1.png)

&emsp;


