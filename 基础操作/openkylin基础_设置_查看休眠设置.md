# openkylin基础 设置 查看休眠设置
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

设置 - 系统 - 电源

![image](images/ok-screen-sleep-1.png)

在电源界面中可以查看休眠设置

&emsp;


