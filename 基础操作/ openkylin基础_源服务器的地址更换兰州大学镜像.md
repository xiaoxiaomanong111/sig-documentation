# <center>源服务器的地址更换兰州大学镜像</center>
#### <center>作者：lerambo</center>
#### <center>2022-08-04 11:25:00</center>


### 1.输入命令
```bash
sudo pluma /etc/apt/sources.list
```
### 2. 更换文件内容的镜像地址
```bash
#原镜像地址
#deb http://archive.build.openkylin.top/openkylin/ yangtze main cross pty
#deb http://archive.build.openkylin.top/openkylin/ yangtze-security main cross pty
#deb http://archive.build.openkylin.top/openkylin/ yangtze-updates main cross pty


#兰州大学镜像地址
deb http://mirror.lzu.edu.cn/openkylin/ yangtze main cross pty
deb http://mirror.lzu.edu.cn/openkylin/ yangtze-security main cross pty
deb http://mirror.lzu.edu.cn/openkylin/ yangtze-updates main cross pty
```

