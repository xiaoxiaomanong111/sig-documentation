# openkylin基础 工具箱 查看整机信息
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

工具箱 查看整机信息 硬件参数 硬件监测 驱动管理

查看整机信息

![image](images/ok-toolshowpc-1.png)

硬件参数

![image](images/ok-toolshowpc-2.png)

硬件监测

![image](images/ok-toolshowpc-3.png)

驱动管理

![image](images/ok-toolshowpc-4.png)

我用虚拟机装的openkylin，所以在驱动管理界面看到的都是虚拟机软件的驱动名称

&emsp;

