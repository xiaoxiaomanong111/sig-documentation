# openkylin基础 查看软件商店
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

软件商店

![image](images/ok-showss-1.png)

可以方便安全地安装软件

![image](images/ok-showss-2.png)

可以卸载已安装的软件

&emsp;

