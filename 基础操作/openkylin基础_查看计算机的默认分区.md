# openkylin基础 查看计算机的默认分区
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

计算机

![image](images/ok-osfirstfiles-1.png)

可以看到文件系统盘和数据盘

查看根目录

![image](images/ok-osfirstfiles-2.png)

在openkylin文件系统中，每个文件夹都有它的特殊定位与作用，感兴趣的同学可以进行深入地学习。

查看数据盘中的内容

![image](images/ok-osfirstfiles-3.png)

&emsp;

