# openkylin基础 安装过程简记
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

欢迎与选择界面

![image](images/openkylin-install-1.png)

检查盘片是否有错误

![image](images/openkylin-install-2.png)

测试内存

![image](images/openkylin-install-3.png)

使用vmware的话，设置默认分配给虚拟机的磁盘大小为50G

全盘安装与自定义安装

![image](images/ok-install-dipar-1.png)

![image](images/ok-install-dipar-2.png)

注意各个分区的文件系统，这里是有知识点的，感兴趣的同学可以详细研究

![image](images/ok-install-dipar-3.png)

![image](images/ok-install-dipar-4.png)

挂载点

``` c
/
/boot
/data
/backup
/home
/tmp
```

&emsp;

