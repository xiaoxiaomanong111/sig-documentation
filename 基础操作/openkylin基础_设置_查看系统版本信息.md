# openkylin基础 设置 查看系统版本信息
#### 作者：师万物
#### 2022-11-21 22:38:16
#### openKylin-0.7.5-x86_64

&emsp;

左下角logo - 设置

![image](images/ok-osinfo-1.png)

设置 - 系统

![image](images/ok-osinfo-2.png)

系统 - 关于

![image](images/ok-osinfo-3.png)

在这个界面可以查看 版本名称、计算机名和内核等相关信息。

&emsp;


