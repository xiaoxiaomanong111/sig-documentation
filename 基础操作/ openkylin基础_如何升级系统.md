# 如何升级openKylin

## 通过官网下载全新安装

[下载链接](https://www.openkylin.top/downloads/)

## 已经安装 openKylin 的用户通过以下方式升级:

```
sudo apt update
sudo apt full-upgrade
```