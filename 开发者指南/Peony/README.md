# Peony

## 什么是Peony

Peony是UKUI3.0中默认的文件管理器。

Peony以glib、gvfs、gio作为底层，使用Qt来进行重构并以此基础进行改进。Peony大致可以分为以下几个部分：

- peony-qt-core: 基于glib、gvfs、gio的对象和接口
- file-operation: 基于peony-qt-core的文件操作接口
- peony-qt-mode: 基于peony-qt-core和file-operation的视图框架
- peony-extensions: 基于Peony的扩展插件
- peony-qt-ui: 基于上述所有组件及Qt的UI架构。

## 如何开始

- [Peony项目指南](https://gitee.com/openkylin/peony/blob/openkylin/yangtze/GUIDE_ZH_CN.md)
- [Peony项目Wiki](https://github.com/ukui/peony/wiki/peony-structure-zh_CN)
- [Peony-Qt的开发者手册](https://github.com/Yue-Lan/peony-qt_development_document)
- [贡献准则](https://github.com/Yue-Lan/peony-qt_development_document/blob/master/contribution-criteria.md)
- [测试用例代码](https://github.com/Yue-Lan/libpeony-qt-development-examples)
